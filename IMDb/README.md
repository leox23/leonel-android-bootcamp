# IMDb app

🚧Vista Busqueda en construccion🚧

### Pendientes:

- [ ] Aplicar la navegacion entre vistas.
- [ ] Corregir el alto del status bar, no parece ser un action var o algo.
- [ ] Validacion de campos en login y register.
- [ ] Trabajar las dimensiones desde res/values/dimens.xml.

### Bugs

<hr/>

### Listos recientemente

- [x] Colocar los textos de vista Login en res/values/strings.xml.
- [x] Trabajar la vista de modo de busqueda tipo AdroidView con recycleView (estructura).
- [x] Modificar entorno para que pueda recibir activities y otros detalles (muchas inconsistencias porque el projecto fue creado en compose)








