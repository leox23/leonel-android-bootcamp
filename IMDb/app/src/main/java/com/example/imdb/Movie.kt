package com.example.imdb

data class Movie(
    val id: Int,
    val title: String,
    val age : Int,
    val description: String,
    val featuredImage : String
)
