package com.example.imdb.ui.theme

import androidx.compose.ui.graphics.Color
import com.example.imdb.R


val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Mustard = Color(0xFFF6C700)
val Black = Color(0xFF000000)
val Grey = Color(0xFF9D9C9C)
val Charcoal = Color(0xFF4B4747)
val White_Smoke = Color(0xFFF5F5F5)

/*
<color name="mustard">#FFF6C700</color>
<color name="black">#FF000000</color>
<color name="grey">#FF9D9C9C</color>
<color name="charcoal">#FF4B4747</color>
<color name="white_smoke">#FFF5F5F5</color>
*/