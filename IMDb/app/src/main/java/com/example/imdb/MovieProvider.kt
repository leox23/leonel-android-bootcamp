package com.example.imdb


import com.example.imdb.R


class MovieProvider {
    companion object {

        val movieList = listOf(
                Movie(
                    1,
                    "Godzilla vs Kong",
                    2022,
                    "Godzilla y Kong, dos de las fuerzas más poderosas de un...",
                   "https://image.tmdb.org/t/p/w185_and_h278_bestv2/bnuC6hu7AB5dYW26A3o6NNLlIlE.jpg"
                ),
                Movie(
                    2,
                    "Spider-Man: Sin camino a casa",
                    2022,
                    "Peter Parker está desenmascarado y ya no puede separar...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/1z7ZQhga4AcSkQNqEU3Tf9XpxoX.jpg"
                ),
                Movie(
                    3,
                    "Luca",
                    2022,
                    "Ambientada en un precioso pueblo costero de la ...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/jTswp6KyDYKtvC52GbHagrZbGvD.jpg"
                ),
                Movie(
                    4,
                    "Cruella",
                    2022,
                    "«Cruella» se sumerge en la juventud rebelde de...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/5bsApWAUqb0jeXtHPjKQ6MZOtZJ.jpg"
                ),
                Movie(
                    5,
                    "La Liga de la Justicia de Zack Snyder",
                    2022,
                    "Decidido a garantizar que el último sacrificio de...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/pI25ennflmJ1R9q7ZTI681f0WBb.jpg"
                ),
                Movie(
                    6,
                    "Encanto",
                    2022,
                    "Encanto, te traslada a Colombia, donde...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/pZc0GpA8jPTwqHuABnSW6XwvWAi.jpg"
                ),
                Movie(
                    7,
                    "Avengers: Endgame",
                    2022,
                    "Tras los sucesos de «Vengadores: Infinity War», los...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/qwLbQSeFy6ht8skBtao7lAZjsDo.jpg"
                ),
                Movie(
                    8,
                    "Fast & Furious 9",
                    2022,
                    "Domic Toretto lleva una vida tranquila, lejos...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/bOFaAXmWWXC3Rbv4u4uM9ZSzRXP.jpg"
                ),
                Movie(
                    9,
                    "Raya y el último dragón",
                    2022,
                    "Raya, una niña de gran espíritu aventurero, se...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/lEC7RHYJ7zidupb9TGJdjwiK4L4.jpg"
                ),
                Movie(
                    10,
                    "Joker",
                    2022,
                    "Situada en los años 80′. Un cómico fallido...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/v0eQLbzT6sWelfApuYsEkYpzufl.jpg"
                ),
                Movie(
                    11,
                    "Venom 2: Habrá Matanza",
                    2022,
                    "Secuela de la película «Venom», el...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/hwzzcppAz4MaBZM061XYJZlYp9S.jpg"
                ),
                Movie(
                    12,
                    "Mortal Kombat",
                    2022,
                    "Un boxeador fracasado descubre un secreto...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/3tZwNIVrax1IV5BkGzWTYMmjYI5.jpg"
                ),
                Movie(
                    13,
                    "El Escuadrón Suicida",
                    2022,
                    "Un grupo de super villanos se encuentran...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/4AcvqhsaE26hshLKZhXmnlsIkcq.jpg"
                ),
                Movie(
                    14,
                    "Soul",
                    2022,
                    "¿Alguna vez te has preguntado de dónde...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/jl8rZYy7JmbWlBlvDCfwvDmGO0g.jpg"
                ),
                Movie(
                    15,
                    "Harry Potter y la piedra filosofal",
                    2022,
                    "Harry Potter es un huérfano que vive con...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/7xXJ15VEf7G9GdAuV1dO769yC73.jpg"
                ),
                Movie(
                    16,
                    "Space Jam 2: Nuevas Leyendas",
                    2022,
                    "Secuela de la cinta original de 1996, Space J...",
                    "https://image.tmdb.org/t/p/w185_and_h278_bestv2/i6E8fx8lAEI0PGGCUlaA2Ap1gWi.jpg"
                )
            )
    }
}

